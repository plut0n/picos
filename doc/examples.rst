.. _examples:

Examples
========

.. toctree::
   :maxdepth: 2

   graphs.rst
   complex.rst
   optdes.rst
