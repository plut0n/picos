.. _api:

API Reference
=============

PICOS is organized in a number of submodules, most of which you do not need to
access directly when solving optimization problems. It is usually sufficient to
``import picos`` and use the functions and classes provided in the :mod:`picos`
namespace. Additional utilities can be found in :mod:`picos.tools`.

.. toctree::
   :maxdepth: 1
   :glob:

   api/picos.rst
   api/*
