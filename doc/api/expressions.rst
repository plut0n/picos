picos.expressions
=================

This module contains the expression types created by PICOS when you perform
algebraic operations on variables and parameters. You do not need to create
expressions directly; it is more convenient to use standard Python operators
(see the :ref:`cheatsheet`) and additional algebraic functions (see the
:mod:`picos` and :mod:`picos.tools` namespaces) on the basic affine expressions
created by :func:`add_variable <picos.Problem.add_variable>` and
:func:`new_param <picos.new_param>`.

Members
-------

.. automodule:: picos.expressions
  :members:
  :undoc-members:
