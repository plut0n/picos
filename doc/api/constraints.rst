picos.constraints
=================

This package contains the constraint types that are used to express optimization
constraints.
You do not need to instanciate these constraints directly; it is more convenient
to create them by applying Python's comparison operators to algebraic
expressions (see the :ref:`cheatsheet`).

.. module:: picos.constraints
.. automodapi:: picos.constraints
  :no-heading:
  :headings: =-
  :include-all-objects:
  :no-main-docstr:
  :no-inheritance-diagram:
