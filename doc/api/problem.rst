picos.problem
=============

The :class:`Problem <picos.Problem>` class represents your optimization problem
and is your main way of interfacing PICOS. After you create a problem instance,
you can add variables to it via :meth:`add_variable
<picos.Problem.add_variable>` and use standard Python algebraic operators
(:ref:`cheatsheet`) and algebraic functions (:mod:`picos.tools`) to create
expressions and constraints involving these variables.

Members
-------

.. automodule:: picos.problem
  :members:
  :undoc-members:
