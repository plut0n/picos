picos.tools
===========

Many of the tools, in particular the algebraic functions, are also available
in the :mod:`picos` namespace. For example, you can write :func:`picos.sum
<picos.sum>` instead of :func:`picos.tools.sum <picos.tools.sum>`.
In the future, we are looking to split the toolbox into mutltiple modules, so
that it is clear which of the functions are imported into the :mod:`picos`
namespace.

Members
-------

.. automodule:: picos.tools
  :members:
  :undoc-members:
