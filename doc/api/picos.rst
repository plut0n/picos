.. module:: picos

picos
=====

The :mod:`picos` namespace gives you quick access to the most important classes
and functions for optimizing with PICOS, so that ``import picos`` is often
sufficient for implementing your model.
You can find additional utilities in the :mod:`picos.tools` namespace.

.. automodapi:: picos
  :no-heading:
  :headings: =-
  :include-all-objects:
  :no-main-docstr:
  :no-inheritance-diagram:
