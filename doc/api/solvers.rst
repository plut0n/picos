picos.solvers
=============

This package contains the interfaces to the optimization solvers that PICOS uses
as its backend.
You do not need to instanciate any of the solver classes directly; if you want
to select a particular solver, it is most convenient to supply it to
:func:`Problem.solve <picos.Problem.solve>` via the ``solver`` keyword argument.

.. module:: picos.solvers
.. automodapi:: picos.solvers
  :no-heading:
  :headings: =-
  :include-all-objects:
  :no-main-docstr:
  :no-inheritance-diagram:
