.. _glyphs:

picos.glyphs
============

PICOS internally uses this module to produce string representations for the
algebraic expressions that you create.
The function-like objects that are used to build such strings are called
"glyphs" and are instanciated by this module following the
`singleton pattern <https://en.wikipedia.org/wiki/Singleton_pattern>`_.
As a result, you can modify the glyph objects listed below to influence how
PICOS will format future strings, for example to disable use of unicode symbols
that your console does not suppport or to adapt PICOS' output to the rest of
your application.

Here's an example of first swapping the entire character set to display
expressions using only `Latin-1 <https://en.wikipedia.org/wiki/ISO/IEC_8859-1>`_
characters, and then modifying a single glyph to our liking:

  >>> import picos
  >>> X = picos.new_problem().add_variable("X", (2,2), "symmetric")
  >>> print(X >> 0)
  X ≽ 0
  >>> picos.glyphs.latin1()
  >>> print(X >> 0)
  X » 0
  >>> picos.glyphs.psdge.template = "{} - {} is psd"
  >>> print(X >> 0)
  X - 0 is psd

Note that glyphs understand some algebraic rules such as operator precedence
and associativity. This is possible because strings produced by glyphs remember
how they were created.

  >>> one_plus_two = picos.glyphs.add(1, 2)
  >>> one_plus_two
  '1 + 2'
  >>> one_plus_two.glyph.template, one_plus_two.operands
  ('{} + {}', (1, 2))
  >>> picos.glyphs.add(one_plus_two, 3)
  '1 + 2 + 3'
  >>> picos.glyphs.sub(0, one_plus_two)
  '0 - (1 + 2)'

The positive semidefinite glyph above does not yet know how to properly handle
arguments with respect to the ``-`` symbol involved, but we can modify it
further:

  >>> print(X + X >> X + X)
  X + X - X + X is psd
  >>> # Use the same operator binding strength as regular substraction.
  >>> picos.glyphs.psdge.order = picos.glyphs.sub.order
  >>> print(X + X >> X + X)
  X + X - (X + X) is psd

You can reset all glyphs to their initial state as follows:

  >>> picos.glyphs.default()

Members
-------

.. automodule:: picos.glyphs
  :members:
  :undoc-members:
