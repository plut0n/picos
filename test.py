#!/usr/bin/python

import sys
import argparse
import unittest
import picos
import picos.tests

def str2dict(string):
    import ast
    if string == "_":
        return {}
    dictionary = {}
    pairs = string.split(";")
    for pair in pairs:
        if "=" in pair:
            key, val = pair.split("=", 1)
            dictionary[key] = ast.literal_eval(val)
        else:
            key, val = pair.split(":", 1)
            dictionary[key] = val
    return dictionary

defaultProductionTestOptions = picos.tests.ptest.ProductionTestCase.Options()

parser = argparse.ArgumentParser(description="Test runner for PICOS.")
group = parser.add_mutually_exclusive_group()
group.add_argument("-u", "--unit", action="store_true",
    help="run only unit tests")
group.add_argument("-p", "--production", action="store_true",
    help="run only production tests")
parser.add_argument("-v", action="count", dest="testingVerbosity", default=0,
    help="increase testing verbosity, repeatable")
group = parser.add_mutually_exclusive_group()
group.add_argument("-d", action="count", dest="testVerbosity", default=0,
    help="increase test verbosity for debugging, repeatable")
group.add_argument("-q", action="store_const", const=-1, dest="testVerbosity",
    help="suppress test output, including warnings")
parser.add_argument("-l", "--list", action="store_true",
    help="list available filter options")
parser.add_argument("-s", "--solvers", metavar="SOLVER", nargs="+",
    default=picos.solvers.available_solvers(),
    help="select a subset of solvers to test")
parser.add_argument("-t", "--testsets", metavar="SET", nargs="+",
    help="select a subset of test sets to run")
parser.add_argument("-c", "--testcases", metavar="CASE", nargs="+",
    help="select a subset of test cases to run from each set")
parser.add_argument("-n", "--testnames", metavar="TEST", nargs="+",
    help="select a subset of tests to run from each test case")
parser.add_argument("-m", "--minsupport", metavar="LEVEL", type=int,
    default=picos.solvers.SUPPORT_LEVEL_SECONDARY,
    help="sets the required solver/problem support level")
parser.add_argument("-b", "--solveboth", action="store_true",
    help="always ask solver for primal and dual solution")
parser.add_argument("-o", "--options", metavar="SET", nargs="+", type=str2dict,
    default=[{}], help="define a set of solver options to be tested, each given"
    " either as _, the empty set, or as a sequence key1:val1;key2=val2 where "
    "val1 is a string literal and val2 will be evaluated to a Python literal")
group = parser.add_mutually_exclusive_group()
# TODO: Find a working HTML test runner and add it to the group.
group.add_argument("-x", "--xml", action="store_true",
    help="produce XML output via the xmlrunner package")
group = parser.add_mutually_exclusive_group()
group.add_argument("-f", "--forks", metavar="NUM", type=int, default=0,
    help="number of tests to run in parallel via the concurrencytest package, "
    "-1 for number of cpu threads")
group.add_argument("-F", dest="forks", action="store_const", const=-1,
    help="equals -f -1, create number of cpu threads many forks")
parser.add_argument("--objplaces", metavar="NUM", type=int,
    default=defaultProductionTestOptions.objPlaces,
    help="number of decimal places after the point to consider when comparing "
    "objective values")
parser.add_argument("--varplaces", metavar="NUM", type=int,
    default=defaultProductionTestOptions.varPlaces,
    help="number of decimal places after the point to consider when comparing "
    "variable values")

args = parser.parse_args()

if args.list:
    solvers = picos.solvers.available_solvers()
    sets, cases, tests = picos.tests.ptest.makeTestSuite(listSelection = True)
    print("Available solvers (-s):")
    print("  " + ", ".join(solvers))
    print("\nProduction test sets (-t):")
    print("  " + ", ".join(sorted(sets)))
    print("\nProduction test cases (-c):")
    print("  " + ", ".join(sorted(cases)))
    print("\nProduction test names (-n):")
    print("  " + ", ".join(sorted(tests)))
    print('\nFilters are case-insensitive and the "test" prefix can be ommitted'
        ' for test names.')
    quit()

# Create a suite of all selected tests.
testSuite = unittest.TestSuite()
if not args.unit:
    testOptions = picos.tests.ptest.ProductionTestCase.Options()
    testOptions.verbosity = args.testVerbosity
    if args.objplaces is not None:
        testOptions.objPlaces = args.objplaces
    if args.varplaces is not None:
        testOptions.varPlaces = args.varplaces
    if args.minsupport is not None:
        testOptions.minSupport = args.minsupport
    testOptions.solveBoth = args.solveboth
    productionTestSuite = picos.tests.ptest.makeTestSuite(
        testSetFilter = args.testsets, testCaseFilter = args.testcases,
        testFilter = args.testnames, solvers = args.solvers,
        solverOptionSets = args.options, testOptions = testOptions)
    testSuite.addTest(productionTestSuite)

# Decide on a test runner to use.
if args.xml:
    import xmlrunner
    testRunner = xmlrunner.XMLTestRunner(
        output = "xml-reports", verbosity = args.testingVerbosity)
else:
    testRunner = unittest.TextTestRunner(verbosity = args.testingVerbosity)

# Parallelize if requested.
if args.forks:
    if args.forks < 0:
        import multiprocessing
        numForks = multiprocessing.cpu_count()
    else:
        numForks = args.forks
    import concurrencytest
    testSuite = concurrencytest.ConcurrentTestSuite(
        testSuite, concurrencytest.fork_for_tests(numForks))

result   = testRunner.run(testSuite)
failures = len(result.failures)
errors   = len(result.errors)
retval   = ((failures > 0) << 0) + ((errors > 0) << 1)

sys.exit(retval)
