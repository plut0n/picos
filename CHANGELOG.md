# Changelog

This file documents major changes to PICOS.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

[Unreleased]: https://gitlab.com/picos-api/picos/compare/v1.2.0...master
[1.2.0]: https://gitlab.com/picos-api/picos/compare/v1.1.3...v1.2.0
[1.1.3]: https://gitlab.com/picos-api/picos/compare/v1.1.2...v1.1.3
[1.1.2]: https://gitlab.com/picos-api/picos/compare/v1.1.1...v1.1.2
[1.1.1]: https://gitlab.com/picos-api/picos/compare/v1.1.0...v1.1.1
[1.1.0]: https://gitlab.com/picos-api/picos/compare/v1.0.2...v1.1.0
[1.0.2]: https://gitlab.com/picos-api/picos/compare/v1.0.1...v1.0.2
[1.0.1]: https://gitlab.com/picos-api/picos/compare/v1.0.0...v1.0.1
[1.0.0]: https://gitlab.com/picos-api/picos/compare/b65a05be...v1.0.0
[0.1.3]: about:blank
[0.1.2]: about:blank
[0.1.1]: about:blank
[0.1.0]: about:blank


## [Unreleased]

### Added
- Support for MOSEK 9.
- Support for ECOS 2.0.7.
- Support for multiple subsystems with
  :func:`partial_trace <picos.tools.partial_trace>`.
- Possibility to use :func:`picos.sum` to sum over the elements of a single
  multidimensional expression.

### Fixed
- Upgrading the PyPI package via pip.
- A regression that rendered the Kronecker product unusable.
- Noisy exception handling in :func:`spmatrix <picos.tools.spmatrix>`.
- Shape detection for matrices given by string.
- The ``hotstart`` option when solving with CPLEX.


## [1.2.0] - 2019-01-11

### Important
- :attr:`A scalar expression's value <picos.expressions.Expression.value>` and
  :attr:`a scalar constraint's dual <picos.constraints.Constraint.dual>` are
  returned as scalar types as opposed to 1×1 matrices.
- The dual value returned for rotated second order cone constraints is now a
  proper member of the dual cone (which equals the primal cone up to a factor of
  :math:`4`). Previously, the dual of an equivalent second order cone constraint
  was returned.
- The Python 2/3 compatibility library ``six`` is no longer a dependency.

### Added
- Support for the ECOS solver.
- Experimental support for MOSEK's new Fusion API.
- Full support for exponential cone programming.
- A production testing framework featuring around 40 novel optimization test
  cases that allows quick selection of tests, solvers, and solver options.
- A "glyph" system that allows the user to adjust the string representations of
  future expressions and constraints. For instance,
  :func:`picos.latin1() <picos.glyphs.latin1>` disables use of unicode symbols.
- Support for symmetric variables with all solvers, even if they do not support
  semidefinite programming.

### Changed
- Solver implementations each have a source file of their own, and a common
  interface that makes implementing new solvers easier.
- Likewise, constraint implementations each have a source file of their own.
- The implementations of CPLEX, Gurobi, MOSEK and SCIP have been rewritten.
- Solver selection takes into account how well a problem is supported,
  distinguishing between native, secondary, experimental and limited support.
- Unsupported operations on expressions now produce meaningful exceptions.
- :func:`add_constraint <picos.Problem.add_constraint>` and
  :func:`add_list_of_constraints <picos.Problem.add_list_of_constraints>`
  always return the constraints
  passed to them.
- :func:`add_list_of_constraints <picos.Problem.add_list_of_constraints>`
  and :func:`picos.sum <picos.tools.sum>` find a short string representation
  automatically.

### Removed
- The old production testing script.
- Support for the SDPA solver.
- Support for sequential quadratic programming.
- The options ``convert_quad_to_socp_if_needed``, ``pass_simple_cons_as_bound``,
  ``return_constraints``, ``handleBarVars``, ``handleConeVars`` and
  ``smcp_feas``.
- Support for GLPK and MOSEK through CVXOPT.

### Fixed
- Performance issues when exporting variable bounds to CVXOPT.
- Hadamard product involving complex matrices.
- Adding constant terms to quadratic expression.
- Incorrect or redundant expression string representations.
- GLPK handling of the default ``maxit`` option.
- Miscellaneous solver-specific bugs in the solvers that were re-implemented.


## [1.1.3] - 2018-10-05

### Added
- Support for the solvers GLPK and SCIP.
- PICOS packages [on Anaconda Cloud](https://anaconda.org/picos/picos).
- PICOS packages
  [in the Arch Linux User Repository](https://aur.archlinux.org/packages/?SeB=b&K=python-picos).

### Changed
- The main repository has moved to [GitLab](https://gitlab.com/picos-api/picos).
- Releases of packages and documentation changes are
  [automated](https://about.gitlab.com/features/gitlab-ci-cd/) and thus more
  frequent. In particular, post release versions are available.
- Test bench execution is automated for greater code stability.
- Improved test bench output.
- Improved support for the SDPA solver.
- :func:`partial_trace <picos.tools.partial_trace>` can handle rectangular
  subsystems.
- The documentation was restructured; examples were converted to Python 3.

### Fixed
- Upper bounding the norm of a complex scalar.
- Multiplication with a complex scalar.
- A couple of Python 3 specific errors, in particular when deleting constraints.
- All documentation examples are reproducible with the current state of PICOS.


## [1.1.2] - 2016-07-04

### Added
- Ability to dynamically add and remove constraints, see
  :ref:`the documentation on constraint deletion <delcons>`.
- Option ``pass_simple_cons_as_bound``, see below.

### Changed
- Improved efficiency when processing large expressions.
- Improved support for the SDPA solver.
- :func:`add_constraint <picos.Problem.add_constraint>` returns a handle to the
  constraint when the option `return_constraints` is set.
- New signature for the function
  :func:`partial_transpose <picos.tools.partial_transpose>`, which can now
  transpose arbitrary subsystems from a kronecker product.
- PICOS no longer turns constraints into variable bounds, unless the new option
  ``pass_simple_cons_as_bound`` is enabled.

### Fixed
- Minor bugs with complex expressions.


## [1.1.1] - 2015-08-29

### Added
- Support for the SDPA solver.
- Partial trace of an affine expression, see
  :func:`partial_trace <picos.tools.partial_trace>`.

### Changed
- Improved PEP 8 compliance.

### Fixed
- Compatibility with Python 3.


## [1.1.0] - 2015-04-15

### Added
- Compatibility with Python 3.

### Changed
- The main repository has moved to [GitHub](https://github.com/gsagnol/picos).


## [1.0.2] - 2015-01-30

### Added
- Ability to read and write problems in
  [conic benchmark format](http://cblib.zib.de/).
- Support for inequalities involving the sum of the :math:`k` largest or
  smallest elements of an affine expression, see
  :func:`sum_k_largest <picos.tools.sum_k_largest>` and
  :func:`sum_k_smallest <picos.tools.sum_k_smallest>`.
- Support for inequalities involving the sum of the :math:`k` largest or
  smallest eigenvalues of a symmetric matrix, see
  :func:`sum_k_largest_lambda <picos.tools.sum_k_largest_lambda>`,
  :func:`sum_k_smallest_lambda <picos.tools.sum_k_smallest_lambda>`,
  :func:`lambda_max <picos.tools.lambda_max>` and
  :func:`lambda_min <picos.tools.lambda_min>`.
- Support for inequalities involving the :math:`L_{p,q}`-norm of an affine
  expression, see :func:`norm <picos.tools.norm>`.
- Support for equalities involving complex coefficients.
- New :attr:`variable type <picos.expressions.Variable.vtype>` for antisymmetric
  matrix variables: ``antisym``.
- Set expressions that affine expressions can be constrained to be an element
  of, see :func:`ball <picos.tools.ball>`, :func:`simplex <picos.tools.simplex>`
  and :func:`truncated_simplex <picos.tools.truncated_simplex>`.
- Shorthand functions :func:`maximize <picos.Problem.maximize>` and
  :func:`minimize <picos.Problem.minimize>` to specify the objective function of
  a problem and solve it.
- Hadamard (elementwise) product of affine expression, as an overload of the
  ``^`` operator, read :ref:`the tutorial on overloads <overloads>`.
- Partial transposition of an aAffine Expression, see
  :func:`partial_transpose <picos.tools.partial_transpose>` and
  :attr:`AffinExp.Tx <picos.expressions.AffinExp.Tx>`.

### Changed
- Improved efficiency of the sparse SDPA file format writer.
- Improved efficiency of :func:`to_real <picos.Problem.as_real>`.

### Fixed
- Scalar product of hermitian matrices.
- Conjugate of a complex expression.


## [1.0.1] - 2014-08-27

### Added
- Support for semidefinite programming over the complex domain, see
  :ref:`the documentation on complex problems <complex>`.
- Helper function to input (multicommodity) graph flow problems, see
  :ref:`the tutorial on flow constraints <flowcons>`.
- Additional ``coef`` argument to :func:`tracepow <picos.tools.tracepow>`, to
  represent constraints of the form
  :math:`\operatorname{trace}(M X^p) \geq t`.

### Changed
- Significantly improved slicing performance for
  :class:`affine expressions <picos.expressions.AffinExp>`.
- Improved performance of
  :func:`retrieve_matrix <picos.tools.retrieve_matrix>`.
- Improved performance when retrieving primal solution from CPLEX.
- The documentation received an overhaul.


## [1.0.0] - 2013-07-19

### Added
- Ability to express rational powers of affine expressions with the ``**``
  operator, traces of matrix powers with
  :func:`picos.tracepow <picos.tools.tracepow>`, (generalized) p-norms with
  :func:`picos.norm <picos.tools.norm>` and :math:`n`-th roots of a determinant
  with :func:`picos.detrootn <picos.tools.detrootn>`.
- Ability to specify variable bounds directly rather than by adding constraints,
  see :func:`add_variable <picos.Problem.add_variable>`,
  :func:`set_lower() <picos.expressions.Variable.set_lower>`,
  :func:`set_upper() <picos.expressions.Variable.set_upper>`,
  :func:`set_sparse_lower() <picos.expressions.Variable.set_sparse_lower>` and
  :func:`set_sparse_upper() <picos.expressions.Variable.set_sparse_upper>`.
- Problem dualization, see :func:`dualize <picos.Problem.as_dual>`.
- Option ``solve_via_dual`` which controls passing the dual problem to the
  solver instead of the primal problem. This can result in a significant
  speedup for certain problems.
- Semidefinite programming interface for MOSEK 7.0.
- Options ``handleBarVars`` and ``handleConeVars`` to customize how SOCPs and
  SDPs are passed to MOSEK. When these are set to ``True``, PICOS tries to
  minimize the number of variables of the MOSEK instance.

### Changed
- If the chosen solver supports this, updated problems will be partially
  re-solved instead of solved from scratch.

### Removed
- Option ``onlyChangeObjective``.


## [0.1.3] - 2013-04-17

### Added
- A :func:`geomean <picos.tools.geomean>` function to construct geometric mean
  inequalities that will be cast as SOCP constraints.
- Options ``uboundlimit`` and ``lboundlimit`` to tell CPLEX to stop the search
  as soon as the given threshold is reached for the upper and lower bound,
  respectively.
- Option ``boundMonitor`` to inspect the evolution of CPLEX lower and upper
  bounds.
- Ability to use the weak inequality operators as an alias for the strong ones.

### Changed
- The solver search time is returned in the dictionary returned by
  :func:`solve <picos.Problem.solve>`.

### Fixed
- Access to dual values of fixed variables with CPLEX.
- Evaluation of constant affine expressions with a zero coefficient.
- Number of constraints not being updated in
  :func:`remove_constraint <picos.Problem.remove_constraint>`.


## [0.1.2] - 2013-01-10

### Fixed
- Writing SDPA files. The lower triangular part of the constraint matrix was
  written instead of the upper triangular part.
- A wrongly raised :class:`IndexError` from
  :func:`remove_constraint <picos.Problem.remove_constraint>`.


## [0.1.1] - 2012-12-08

### Added
- Interface to Gurobi.
- Ability to give an initial solution to warm-start mixed integer optimizers.
- Ability to get a reference to a constraint that was added.

### Fixed
- Minor bugs with quadratic expressions.


## [0.1.0] - 2012-06-22

### Added
- Initial release of PICOS.
